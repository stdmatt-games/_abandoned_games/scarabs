
const PLAYER_SCALE = 1.0;
const PLAYER_SHADOW_SCALE_MULTIPLIER = 0.9
const PLAYER_SHADOW_MIN_DISTANCE  = 20;
const PLAYER_SHADOW_MAX_DISTANCE  = 60;
const PLAYER_MOVE_ANIMATION_DURATION = 200;
const PLAYER_DEATH_ANIMATION_DURATION = 400;

class Player
{
    //--------------------------------------------------------------------------
    constructor(x, y, w, h)
    {
        this.pos         = pw_Vector_Create(x, y);
        this.size        = pw_Vector_Create(w, h);
        this.start_pos   = null;
        this.target_pos  = null;
        this.scale       = PLAYER_SCALE;

        // Move
        this.move_tween           = null;
        this.is_moving            = false;
        this.just_finished_moving = false;

        // Die
        this.die_tween           = null;
        this.is_dying            = false;
        this.just_finished_dying = false;
    } // CTOR

    //--------------------------------------------------------------------------
    GetDesiredDirection()
    {
        if(this.is_moving || this.is_dying || this.just_finished_dying) {
            return null;
        }

        if(pw_Keyboard_IsClick(PW_KEY_ARROW_LEFT)) {
            return pw_Vector_Left();
        } else if(pw_Keyboard_IsClick(PW_KEY_ARROW_RIGHT)) {
            return pw_Vector_Right();
        } else if(pw_Keyboard_IsClick(PW_KEY_ARROW_UP)) {
            return pw_Vector_Up();
        } else if(pw_Keyboard_IsClick(PW_KEY_ARROW_DOWN)) {
            return pw_Vector_Down();
        }
        return null;
    }

    //--------------------------------------------------------------------------
    MoveToPos(x, y)
    {
        this.move_tween = pw_Tween_CreateBasic(PLAYER_MOVE_ANIMATION_DURATION)
            .onStart(()=>{
                this.is_moving  = true;
                this.start_pos  = pw_Vector_Copy(this.pos);
                this.target_pos = pw_Vector_Create(x, y);
            })
            .onUpdate((v)=>{
                this.scale = PLAYER_SCALE + pw_Math_Sin(PW_MATH_PI * v.value);
                this.pos.x = pw_Math_Lerp(this.start_pos.x, this.target_pos.x, v.value);
                this.pos.y = pw_Math_Lerp(this.start_pos.y, this.target_pos.y, v.value);
            })
            .onComplete(()=>{
                this.pos                  = this.target_pos;
                this.is_moving            = false;
                this.just_finished_moving = true;
            })
            .start();
    } // MoveToPos

    //--------------------------------------------------------------------------
    Die()
    {
        this.die_tween = pw_Tween_CreateBasic(PLAYER_DEATH_ANIMATION_DURATION)
            .onStart(()=>{
                this.is_dying = true;
            })
            .onUpdate((v)=>{
                this.scale = PLAYER_SCALE - pw_Math_Sin(PW_MATH_PI_OVER_2 * v.value);
                console.log(this.scale)
            })
            .onComplete(()=>{
                this.is_dying            = false;
                this.just_finished_dying = true;
            })
            .start();
    }


    //--------------------------------------------------------------------------
    Update()
    {
    } // Update

    //--------------------------------------------------------------------------
    Draw()
    {
        if(this.is_moving) {
            pw_Canvas_Push();
                const shadow_scale = (this.scale - 1) * PLAYER_SHADOW_SCALE_MULTIPLIER;
                if(shadow_scale > 0) {
                    const shadow_dist = pw_Math_Map(
                        shadow_scale,
                        0,
                        PLAYER_SHADOW_SCALE_MULTIPLIER,
                        PLAYER_SHADOW_MIN_DISTANCE,
                        PLAYER_SHADOW_MAX_DISTANCE
                    );
                    const shadow_alpha = pw_Math_Map(
                        shadow_scale,
                        0,
                        PLAYER_SHADOW_SCALE_MULTIPLIER,
                        1.0,
                        0.5
                    );
                    const color = MakeRGBA(0x28, 0x28, 0x28, shadow_alpha);
                    pw_Canvas_Translate(this.pos.x + shadow_dist, this.pos.y + shadow_dist);
                    pw_Canvas_Scale(shadow_scale);
                    pw_Canvas_SetFillStyle(color);
                    pw_Canvas_FillCircle(0, 0, this.size.x * 0.5); // @fix why use w,h??
                }

            pw_Canvas_Pop();
        }

        pw_Canvas_Push();
            pw_Canvas_Translate(this.pos.x, this.pos.y);
            pw_Canvas_Scale(this.scale);
            pw_Canvas_SetFillStyle("red");
            pw_Canvas_FillCircle(0, 0, this.size.x * 0.5); // @fix why use w,h??
        pw_Canvas_Pop()
    } // Draw

} // Class Player

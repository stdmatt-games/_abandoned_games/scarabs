
//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function
InitializeCanvas()
{
    //
    // Configure the Canvas.
    const parent        = document.getElementById("canvas_div");
    const parent_width  = parent.clientWidth;
    const parent_height = parent.clientHeight;

    const max_side = pw_Math_Max(parent_width, parent_height);
    const min_side = pw_Math_Min(parent_width, parent_height);

    const ratio = min_side / max_side;
    const DESIGN_WIDTH  = 800;
    const DESIGN_HEIGHT = 800;

    // Landscape
    if(parent_width > parent_height) {
        pw_Canvas_CreateCanvas(DESIGN_WIDTH, DESIGN_WIDTH* ratio, parent);
    }
    // Portrait
    else {
        pw_Canvas_CreateCanvas(DESIGN_WIDTH * ratio, DESIGN_HEIGHT, parent);
    }

    pw_Canvas.style.width  = "100%";
    pw_Canvas.style.height = "100%";
}



function
MakeRGBA(r, g, b, a)
{
    return pw_String_Cat("rgba(", r, ",", g, ",", b, ",", a, ")");
}


let CurrGame = null;
//----------------------------------------------------------------------------//
// Setup / Draw                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
async function
Setup()
{
    pw_Random_Seed(null);
    InitializeCanvas();

    pw_Input_InstallBasicMouseHandler(pw_Canvas);
    pw_Input_InstallBasicKeyboardHandler();

    CurrGame = new Game();
    await CurrGame.Reset(0);

    pw_Canvas_Start();
}

//------------------------------------------------------------------------------
function
Draw(dt)
{
    pw_Canvas_ClearWindow("black");
    CurrGame.Update();

    pw_Canvas_Push();
    pw_Canvas_Translate(
        -pw_Canvas_Half_Width  + CurrGame.floor.tile_size.x * 0.5,
        -pw_Canvas_Half_Height + CurrGame.floor.tile_size.y * 0.5
    );
        CurrGame.Draw();

    pw_Canvas_SetFillStyle("blue");
    pw_Canvas_FillCircle(0, 0, 20);
    pw_Canvas_Pop();

    pw_Tween_Update(dt);
    pw_Input_KeyboardEndFrame();
}


//----------------------------------------------------------------------------//
// Input                                                                      //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function
OnKeyDown(event)
{
}

//------------------------------------------------------------------------------
function
OnKeyUp(event)
{
}


//------------------------------------------------------------------------------
function
OnMouseClick()
{
}


//----------------------------------------------------------------------------//
// Entry Point                                                                //
//----------------------------------------------------------------------------//
Setup();
